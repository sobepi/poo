
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
     <title>Fundamentos básicos com PHP</title>
    <link rel="stylesheet" href="style.css" media="all" />

</head>
<body>

<?php 
include_once './Veiculo.php';
include_once './Caminhao.php';
$caminhao = new Caminhao("gasolina", "dfr-5432", "preto", "2018", "6","container","1 T");
$caminhao->mostrarInformacoes();
$veiculo = new Veiculo("Disel", "GRT-6745", "Vermelho", "2016");
$veiculo->mostrarInformacoes();

//$veiculo1 = new Veiculo("Gasolina", "GRT-6745", "Branco", "2014");
//$veiculo1->mostrarInformacoes();

/*$veiculo->setCombustivel("Disel");
$veiculo->setCor("Branco");
$veiculo->setAno("2000");
$veiculo->setPlaca("GTR-5432");

$veiculo1 = new Veiculo();
$veiculo1->setCombustivel("Gasolina");
$veiculo1->setCor("Vermelho");
$veiculo1->setAno("2017");
$veiculo1->setPlaca("GFD-9078");

$veiculo->mostrarInformacoes();

var_dump($veiculo->getAno()."<br>");
var_dump($veiculo->getCombustivel()."<br>");
var_dump($veiculo->getCor()."<br>");
var_dump($veiculo->getPlaca()."<br>");

var_dump($veiculo1->getAno()."<br>");
var_dump($veiculo1->getCombustivel()."<br>");
var_dump($veiculo1->getCor()."<br>");
var_dump($veiculo1->getPlaca()."<br>");*/

?>

</body>
</html>
