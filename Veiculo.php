<?php 

class Veiculo{
    
    public $combustivel;
    public $placa;
    Public $cor;
    public $ano;
    

    public function __construct($combustivel,$placa, $cor, $ano){
        self::setCombustivel($combustivel);
        self::setPlaca($placa);
        self::setCor($cor);
        self::setAno($ano);
        
    } 
        
    
    /**
     * @return mixed
     */
    public function getCombustivel()
    {
        return $this->combustivel;
    }

    /**
     * @return mixed
     */
    public function getPlaca()
    {
        return $this->placa;
    }

    /**
     * @return mixed
     */
    public function getCor()
    {
        return $this->cor;
    }

    /**
     * @return mixed
     */
    public function getAno()
    {
        return $this->ano;
    }

    /**
     * @param mixed $combustivel
     */
    public function setCombustivel($combustivel)
    {
        $this->combustivel = $combustivel;
    }

    /**
     * @param mixed $placa
     */
    public function setPlaca($placa)
    {
        $this->placa = $placa;
    }

    /**
     * @param mixed $cor
     */
    public function setCor($cor)
    {
        $this->cor = $cor;
    }

    /**
     * @param mixed $ano
     */
    public function setAno($ano)
    {
        $this->ano = $ano;
    }

    public function mostrarInformacoes(){
        
       echo "<tr>
                 <td>".$this->combustivel."</td>
                 <td>".$this->placa."</td>
                 <td>".$this->cor."</td>
                 <td>".$this->ano."</td>
              </tr>";
        echo "<tr>
                 <td>".self::getCombustivel()."</td>
                 <td>".self::getPlaca()."</td>
                 <td>".self::getCor()."</td>
                 <td>".self::getAno()."</td>
              </tr>";
    }
   
        public function ligarVeiculo(){
            return "Veiculo ligado";
            
        }
        public function desligarVeiculo(){
            return "Veiculo desligado";
            
        }
        public function pararVeiculo(){
            return "Parar Veiculo";
            
        }
        
    public function __destruct() {
        echo "Objeto".get_class($this)." Finalizado <br>";
    }
    
}
?>