<?php 

include_once './Veiculo.php';

class Caminhao extends Veiculo{

    public $rodas;
    public $carroceria;
    public $peso;
    
    public function __construct($combustivel, $placa, $cor, $ano,$rodas,$carroceria,$peso){
        parent::__construct($combustivel, $placa, $cor, $ano);
        self::setRodas($rodas);
        self::setCarroceria($carroceria);
        self::setPeso($peso);        
    }
    
    /**
     * @return mixed
     */
    public function getRodas()
    {
        return $this->rodas;
    }

    /**
     * @return mixed
     */
    public function getCarroceria()
    {
        return $this->carroceria;
    }

    /**
     * @return mixed
     */
    public function getPeso()
    {
        return $this->peso;
    }

    /**
     * @param mixed $rodas
     */
    public function setRodas($rodas)
    {
        $this->rodas = $rodas;
    }

    /**
     * @param mixed $carroceria
     */
    public function setCarroceria($carroceria)
    {
        $this->carroceria = $carroceria;
    }

    /**
     * @param mixed $peso
     */
    public function setPeso($peso)
    {
        $this->peso = $peso;
    }

    public function carregarMercadoria() {
        var_dump(parent::pararVeiculo()."<br/>");
        echo "Carregar Veiculo";
    }
    
    public function mostrarInformacoes(){
        
        echo "<table style='width:20%'>
                <tr>
                 <td>Combustivel</td>
                 <td>Placa</td>
                 <td>Cor</td>
                 <td>Ano</td>
                 <td>Rodas</td>
                 <td>Carroceria</td>
                 <td>Peso</td>
              </tr>";
        echo "<tr>
                 <td>".self::getCombustivel()."</td>
                 <td>".self::getPlaca()."</td>
                 <td>".self::getCor()."</td>
                 <td>".self::getAno()."</td>
                 <td>".self::getRodas()."</td>
                 <td>".self::getCarroceria()."</td>
                 <td>".self::getPeso()."</td>
              </tr>";
    }
    
}




?>